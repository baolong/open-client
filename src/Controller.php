<?php

namespace think\openClient;
 
use think\App;
use think\openClient\helper\FormHelper;
use think\openClient\helper\QueryHelper;
use think\openClient\helper\SaveHelper;
use think\openClient\helper\DeleteHelper;

/**
 * 标准控制器基类
 * Class Controller
 * @package think\openClient
 */
class Controller extends \think\admin\Controller
{

    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->app->bind('think\openClient\Controller', $this);
    }

    /**
     * 重新定义数据库链接
     * @param $dbQuery
     * @param $input
     * @return QueryHelper|Helper|helper\QueryHelper
     * @throws \think\db\exception\DbException
     */
    public function _dbQuery( $dbQuery, $input = null, $connect=null)
    {
        return QueryHelper::instance()->init($dbQuery, $input,null, $connect);
    }

    /**
     * 快捷表单逻辑器
     * @param Model|BaseQuery|string $dbQuery
     * @param string $template 模板名称
     * @param string $field 指定数据对象主键
     * @param mixed $where 额外更新条件
     * @param array $data 表单扩展数据
     * @return array|boolean
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function _dbForm($dbQuery, string $template = '', string $field = '', $where = [], array $data = [])
    {
        return FormHelper::instance()->init($dbQuery, $template, $field, $where, $data);
    }



    /**
     * 快捷更新逻辑器
     * @param Model|BaseQuery|string $dbQuery
     * @param array $data 表单扩展数据
     * @param string $field 数据对象主键
     * @param mixed $where 额外更新条件
     * @return boolean
     * @throws \think\db\exception\DbException
     */
    protected function _dbSave($dbQuery, array $data = [], string $field = '', $where = []): bool
    {
        return SaveHelper::instance()->init($dbQuery, $data, $field, $where);
    }

    /**
     * 快捷删除逻辑器
     * @param Model|BaseQuery|string $dbQuery
     * @param string $field 数据对象主键
     * @param mixed $where 额外更新条件
     * @return boolean|null
     * @throws \think\db\exception\DbException
     */
    protected function _dbDelete($dbQuery, string $field = '', $where = []): ?bool
    {
        return DeleteHelper::instance()->init($dbQuery, $field, $where);
    }

}