<?php

namespace think\openClient;

use think\admin\Exception;
use think\admin\extend\JsonRpcClient;
use think\admin\Service;


/**
 * 服务端接口处理
 * Class MessageService
 * @package app\data\service
 *
 * ----- 开放接口 -----
 * @method mixed OpenSerSystem static 获取站点配置参数
 * @method mixed OpenSerDb static 服务端数据
 *
 */
class Api extends Service {

    /**
     * 静态初始化对象
     * @param string $name
     * @param array $arguments
     * @return mixed
     * @throws \think\admin\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function __callStatic(string $name, array $arguments)
    {
        [$type, $base] = static::parseName($name);
        if ("{$type}{$base}" !== $name) {
            throw new Exception("抱歉，实例 {$name} 不符合规则！");
        }

        [$appid, $appkey] = [sysconf('openserver.appid'), sysconf('openserver.appkey')];
        $data = ['class' => $name, 'appid' => $appid, 'time' => time(), 'nostr' => uniqid()];
        $data['sign'] = md5("{$data['class']}#{$appid}#{$appkey}#{$data['time']}#{$data['nostr']}");

        // 创建远程连接，默认使用 JSON-RPC 方式调用接口
        $token = enbase64url(json_encode($data, JSON_UNESCAPED_UNICODE));
        $location = "http://www.uc.com/service/api.client/_TYPE_?not_init_session=1&token={$token}";
//        p(str_replace('_TYPE_', 'yar', $location));
        if (class_exists('Yar_Client')) {
            return new \Yar_Client(str_replace('_TYPE_', 'yar', $location));
        } else {
            return new JsonRpcClient(str_replace('_TYPE_', 'jsonrpc', $location));
        }
    }

    /**
     * 解析调用对象名称
     * @param string $name
     * @return array
     */
    private static function parseName(string $name): array
    {
        foreach (['WeChat', 'WeMini', 'WeOpen', 'WePay', 'OpenSer'] as $type) {
            if (strpos($name, $type) === 0) {
                [, $base] = explode($type, $name);
                return [$type, $base, "\\{$type}\\{$base}"];
            }
        }
        return ['-', '-', $name];
    }

}